package com.appsterden.safercity.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appsterden.safercity.R;
import com.appsterden.safercity.fragments.CrimeListFragment;
import com.appsterden.safercity.fragments.ReportCrimeFragment;
import com.appsterden.safercity.fragments.ReportTrafficFragment;
import com.appsterden.safercity.fragments.TrafficFragment;
import com.appsterden.safercity.views.SlidingTabLayout;

public class SlidingTabsBasicFragment extends Fragment {

    static final String LOG_TAG = "SlidingTabsBasicFragment";

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sample, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new SamplePagerAdapter(getActivity().getSupportFragmentManager()));
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    class SamplePagerAdapter extends FragmentPagerAdapter {
        public SamplePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            CharSequence pageTitle = "";
            switch (position) {
                case 0:
                    pageTitle = "CRIME INFO";
                    break;
                case 1:
                    pageTitle = "TRAFFIC INFO";
                    break;
                case 2:
                    pageTitle = "REPORT CRIME";
                    break;
                case 3:
                    pageTitle = "REPORT TRAFFIC";
                    break;
                case 4:
                    pageTitle = "SETTINGS";
                    break;
                default:
                    pageTitle = "";
                    break;
            }
            return pageTitle;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return CrimeListFragment.newInstance();
                case 1:
                    return TrafficFragment.newInstance();
                case 2:
                    return ReportCrimeFragment.newInstance();
                case 3:
                    return ReportTrafficFragment.newInstance();
                case 4:
                    return ReportCrimeFragment.newInstance();
                default:
                    return ReportCrimeFragment.newInstance();
            }
        }

    }
}
