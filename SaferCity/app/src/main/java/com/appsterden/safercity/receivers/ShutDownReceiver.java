package com.appsterden.safercity.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.appsterden.safercity.service.SnapService;

/**
 * Created by Nithin on 10/4/2015.
 */
public class ShutDownReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (intent.getAction().equals(Intent.ACTION_SHUTDOWN)||intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
        {
            SnapService.startSnapService(context);
        }
    }
}