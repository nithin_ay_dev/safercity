package com.appsterden.safercity.application;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by Nithin on 10/4/2015.
 */
public class SaferCityApplication extends Application {

    public static TextToSpeech textToSpeech;

    @Override
    public void onCreate() {
        super.onCreate();
        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "J8NuxFkQuzgBIlrA6kFDRfbYfdUjedehdSO0w3uo", "RNyIluvbLXPjbjLIPglWH5UGRRmc1mRkf0jMMFFp");
        ParseInstallation.getCurrentInstallation().put("user_device_id", getDeviceId(this));
        ParseInstallation.getCurrentInstallation().put("GCMSenderId", "377037296246");
        ParseInstallation.getCurrentInstallation().saveInBackground();

    }

    public static String getDeviceId(Context context) {
        return Settings.Secure
                .getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static TextToSpeech getTextToSpeechInstance() {
        return textToSpeech;

    }

}
