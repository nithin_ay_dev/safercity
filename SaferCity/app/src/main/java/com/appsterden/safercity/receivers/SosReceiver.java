package com.appsterden.safercity.receivers;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.appsterden.safercity.R;
import com.appsterden.safercity.activities.MapsActivity;
import com.appsterden.safercity.application.SaferCityApplication;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Nithin on 10/5/2015.
 */
public class SosReceiver extends ParsePushBroadcastReceiver {

    private static final String TAG = "SosReceiver";
    private static final int NOTIFICATION_ID = 1;

    private boolean isShared;
    private String deviceId;
    Location ipLocation;


    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);

        Bundle extras = intent.getExtras();
        String data = intent.getDataString();
        Log.d(" on push received DATA ", data + "");
        try {
            JSONObject object = new JSONObject(extras.getString("com.parse.Data"));
            Log.d(" on push received DATA ", object + "");
            generateNotification(context, object);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void generateNotification(Context context, JSONObject object) {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);

        try {
            String type = object.getString("type");
            Intent mapIntent = new Intent(context, MapsActivity.class);
            String latitude = object.getString("latitude");
            String longitude = object.getString("longitude");
            mapIntent.putExtra("latitude", latitude);
            mapIntent.putExtra("longitude", longitude);
            PendingIntent pIntent = PendingIntent.getActivity(context, 0, mapIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder frndReqNotification = new NotificationCompat.Builder(context)
                    .setContentTitle("Accept request " + object.getString("name"))
                    .setContentText("Subject")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pIntent)
                    .setAutoCancel(false);
            SaferCityApplication.getTextToSpeechInstance().speak(" SomebodyIs In trouble, They need your help", TextToSpeech.QUEUE_FLUSH, null);
            notificationManagerCompat.notify(1, frndReqNotification.build());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
