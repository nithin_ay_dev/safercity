package com.appsterden.safercity.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;

import com.appsterden.safercity.utils.PhotoHandler;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SnapService extends IntentService {
    private Camera camera;
    private int cameraId = 0;

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startSnapService(Context context) {
        Intent intent = new Intent(context, SnapService.class);
        context.startService(intent);
    }


    public SnapService() {
        super("SnapService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            if (getPackageManager()
                    .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                cameraId = findFrontFacingCamera();
                if (cameraId >= 0) {
                    camera = Camera.open(cameraId);
                    camera.takePicture(null, null,
                            new PhotoHandler(getApplicationContext()));
                    camera.release();
                    camera = null;
                }
                cameraId = findBackFacingCamera();
                if (cameraId >= 0) {
                    camera = Camera.open(cameraId);
                    camera.takePicture(null, null,
                            new PhotoHandler(getApplicationContext()));
                    camera.release();
                    camera = null;
                }
            }
        }
    }

    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                Log.d("SnapService", "Camera found");
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Log.d("SnapService", "Camera found");
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

}
